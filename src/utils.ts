type MocItem = {
	id: number;
	name: string;
};

export function generateData(length: number): readonly MocItem[] {
	const array = Array.from({ length }, (_, i) => i);
	return array.map(
		x => ({
			id: x,
			name: `Item ${x}`
		})
	);
}